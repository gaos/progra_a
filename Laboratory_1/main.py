#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import random
from clases import auto

#funcion que se encarga de mostrar el estado del auto
def estado(vehiculo, recorrido, t):
    print("El auto de motor: {0}, se posee {1} L ".format(vehiculo.get_motor(),
                                                        vehiculo.get_estanque()))
    print("y ha recorrido {0} km en {1} horas".format(recorrido,t))
    print("a una velocidad {0} km/h".format(vehiculo.get_velocimetro()))
    print("% de ruedas:\n{0}\n{1}\n{2}\n{3}".format(vehiculo.get_rueda1(),
                                                    vehiculo.get_rueda2(),
                                                    vehiculo.get_rueda3(),
                                                    vehiculo.get_rueda4()))

#basicamente hace todo ya que no hice funciones por error
def moviendo(vehiculo):

    recorrido = 0

    while vehiculo.get_arranca() == True:

        opc = str(input("presione s para encender el vehiculo:"))
        if opc.upper() == 'S' :

            vehiculo.set_arranque(True)
            t = random.randrange(10) + 1
            i = 1

        else :
            vehiculo.set_arranque(False)

        opc = str(input("Desea rellenar el estanque s para hacer:"))

        if opc.upper() == 'S' :
            vehiculo.set_estanque(vehiculo.get_estanque()-32)

        if(vehiculo.get_arranca() == True):
            vehiculo.set_estanque(vehiculo.get_estanque()*0.01)

        while(i <= t and vehiculo.get_arranca() == True):

            print("Puede usar el auto por {0} horas".format(t))
            v = random.randrange(2,121,5)
            distancia = v
            recorrido = distancia + recorrido

            if vehiculo.get_motor() == 1.2:
                gasto = distancia / 20
            else:
                gasto = distancia / 14

            print("El gasto de la distancia es:", gasto)
            vehiculo.set_velocimetro(v)

            #4 condicionales identicas para cada rueda
            if vehiculo.get_rueda1() <= 10 :
                print("debe detenerse a cambiar la rueda")
                vehiculo.set_rueda1((vehiculo.get_rueda1() -100))
                vehiculo.set_arranque(False)

            if vehiculo.get_rueda2() <= 10 :
                print("debe detenerse a cambiar la rueda")
                vehiculo.set_rueda2((vehiculo.get_rueda2() -100))
                vehiculo.set_arranque(False)

            if vehiculo.get_rueda3() <= 10 :
                print("debe detenerse a cambiar la rueda")
                vehiculo.set_rueda3((vehiculo.get_rueda3() -100))
                vehiculo.set_arranque(False)

            if vehiculo.get_rueda4() <= 10 :
                print("debe detenerse a cambiar la rueda")
                vehiculo.set_rueda4((vehiculo.get_rueda4() -100))
                vehiculo.set_arranque(False)

            #condiciones del gasto de la bencina y desgaste de la rueda
            if vehiculo.get_estanque() >= gasto:
                print("Le quedan {0} horas de viaje".format(t-i))

                vehiculo.set_rueda1((random.randrange(10)+1))
                vehiculo.set_rueda2((random.randrange(10)+1))
                vehiculo.set_rueda3((random.randrange(10)+1))
                vehiculo.set_rueda4((random.randrange(10)+1))

                vehiculo.set_estanque(gasto)

                estado(vehiculo, recorrido,i)

            elif vehiculo.get_estanque() < gasto:
                print("Gasto más bencina")
                vehiculo.set_arranque(False)
                break

            opc = str(input("¿Desea seguir a pesar de su estanque? s continua:"
                            ))
            if opc.upper() == 'S':
                vehiculo.set_arranque(True)
            else:
                vehiculo.set_arranque(False)
            i = i + 1





def main():
    al_azar = random.randrange(2)
    mcqueen = auto(al_azar)
    moviendo(mcqueen)



if __name__ == '__main__':
    main()
