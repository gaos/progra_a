#!/usr/bin/env python3
# -*- coding: utf-8 -*-


class auto:

    def __init__(self, azar):

        if azar == 0:
            self.__motor = 1.2
        else:
            self.__motor = 1.6
        print(self.__motor)

        self.__estanque = 0
        self.__velocimetro = 0
        self.__arranca = True
        self.__rueda1 = 100
        self.__rueda2 = 100
        self.__rueda3 = 100
        self.__rueda4 = 100

    def get_motor(self):
        return self.__motor

    def get_velocimetro(self):
        return self.__velocimetro


    def get_estanque(self):
        return self.__estanque

    def get_arranca(self):
        return self.__arranca


    def get_rueda1(self):
        return self.__rueda1

    def get_rueda2(self):
        return self.__rueda2

    def get_rueda3(self):
        return self.__rueda3

    def get_rueda4(self):
        return self.__rueda4


    def set_motor(self, variable):
        self.__motor = variable
        return self.__motor

    def set_velocimetro(self, variable):
        self.__velocimetro = variable
        return self.__velocimetro

    def set_estanque(self, variable):
        self.__estanque = self.__estanque - variable
        return self.__estanque

    def set_arranque(self, variable):
        self.__arranca = variable
        return self.__arranca

    def set_rueda1(self, variable):
        self.__rueda1 = self.__rueda1 - variable
        return self.__rueda1

    def set_rueda2(self, variable):
        self.__rueda2 = self.__rueda2 - variable
        return self.__rueda2

    def set_rueda3(self, variable):
        self.__rueda3 = self.__rueda3 - variable
        return self.__rueda3

    def set_rueda4(self, variable):
        self.__rueda4 = self.__rueda4 - variable
        return self.__rueda4
