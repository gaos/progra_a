#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import os
#from fichero import abre_archivos
import gi
from biopandas.pdb import PandasPdb

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk

__main__.pymol_argv = ['pymol', '-qc']
import  pymol

def da_nombre(variable):
    nombre_reves = list(variable)
    nombre_reves.reverse()
    nombre = []
    for i in range(len(nombre_reves)):
        if nombre_reves[i] == '/':
            return nombre
        else:
            nombre.append(nombre_reves[i])

def listToString(s):

    # initialize an empty string
    str1 = ""

    # return string
    return (str1.join(s))

def open_file(numero):
    #1 lugar del archivo
    if numero == 1:
        file = open("direccion","r")
        lugar = file.read()
        file.close()
    #2 lugar de la carpeta
    elif numero == 2:
        file = open("direccion2","r")
        lugar = file.read()
        file.close()
    return lugar
