#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from gi.repository import Gtk
from gi.repository import Gio
import sys


class MyWindow(Gtk.Window):

     # constructor for a window (the parent window)
    def __init__(self):
        Gtk.Window.__init__(self, title="Presione para la sorpresa")
        self.set_default_size(200, 200)

        box = Gtk.Box(spacing=6)
        self.add(box)

        button1 = Gtk.Button(label="Mostrar Creditos")
        button1.connect("clicked", self.about_cb)
        box.add(button1)




    def about_cb(self, widget):

        aboutdialog = Gtk.AboutDialog()


        authors = ["Gabriel Rojas"]
        documenters = ["Animelist"]


        aboutdialog.set_program_name("Acerca de los autores")
        aboutdialog.set_copyright(
            "Copyright \xc2\xa9 2020 Gaos_180")
        aboutdialog.set_authors(authors)
        aboutdialog.set_documenters(documenters)
        aboutdialog.set_website("http://www.animeflv.net")
        aboutdialog.set_website_label("Critico de Anime")

        aboutdialog.set_title("")

        aboutdialog.connect("response", self.on_close)
        aboutdialog.show()

    def on_close(self, action, parameter):
        action.destroy()


win = MyWindow()
win.connect("destroy", Gtk.main_quit)
win.show_all()
Gtk.main()
