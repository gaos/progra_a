#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import  pymol
from pymol import cmd
import os
"""
Este se encarga de obtener la imagen
"""
def open_file(numero):
    #1 lugar del archivo
    if numero == 1:
        file = open("direccion","r")
        lugar = file.read()
        file.close()
    #2 lugar de la carpeta
    elif numero == 2:
        file = open("direccion2","r")
        lugar = file.read()
        file.close()
    return lugar


def da_nombre(variable):
    nombre_reves = list(variable)
    nombre_reves.reverse()
    nombre = []
    for i in range(len(nombre_reves)):
        if nombre_reves[i] == '/':
            return nombre
        else:
            nombre.append(nombre_reves[i])

def listToString(s):

    # initialize an empty string
    str1 = ""

    # return string
    return (str1.join(s))
#intenta de ver la carpeta
def ver_carpeta():
    listado = os.popen("ls")
    listas = listado.read()
    listas.split("\n")
    flag = False
    for i in range(len(listas)):
        if listas[i] == 'pngs\n':
            flag = True
    if flag == False:
        os.system("mkdir pngs")



def pnghack(filepath):
    cmd.pretty(filepath)
    cmd.png ("pngs/%s.png" % (filepath))

def crear():
    pdb_file = open_file(1)
    pdb_file = pdb_file.rstrip("\n")
    name = da_nombre(pdb_file)
    name.reverse()
    nombre = listToString(name)
    nombre = nombre.rstrip("\n")
    ver_carpeta()
    pdb_name = nombre
    pymol.finish_launching()

    cmd.set('ray_trace_frames', 1)  # Frames are raytraced before saving an image.
    cmd.load("{0}".format(pdb_file),"{0}".format(pdb_name))
    cmd.disable("all")
    cmd.enable(pdb_name)
    cmd.hide("all")
    cmd.show('cartoon')
    cmd.set('ray_opaque_background' , 0)
    pnghack("{0}".format(pdb_name))
    pymol.cmd.quit()
    print("imagen creada")

crear()
