#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import  __main__
import os
from fichero import open_file
import gi
from biopandas.pdb import PandasPdb
from genera import hacer_imagen

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk

__main__.pymol_argv = ['pymol', '-qc']

import  pymol
#pymol.finish_launching()
"""
esto se encarga de producir un png del archivo
"""

variable = open_file(1)
"""
# Pymol: quiet  and no GUI

"""
def da_nombre():
    nombre_reves = list(variable)
    nombre_reves.reverse()
    nombre = []
    for i in range(len(nombre_reves)):
        if nombre_reves[i] == '/':
            return nombre
        else:
            nombre.append(nombre_reves[i])

def listToString(s):

    # initialize an empty string
    str1 = ""

    # return string
    return (str1.join(s))


def ver_normal():
    name = da_nombre()
    name.reverse()
    nombre_pdb = listToString(name)
    hacer_imagen(nombre_pdb)


def ver_en_pymol():

    if(variable[len(variable) - 1] == 'b' and
        variable[len(variable) - 2] == 'd' and
        variable[len(variable) - 3] == 'p' and
        variable[len(variable) - 4] == '.'
        ):

        os.system("pymol {0}".format(variable))

    else:
        print("Ha escogido un archivo que no es .pdb")

def ver_datos():
    ppdb = PandasPdb()
    ppdb.read_pdb(variable)
    print (ppdb.df.keys())
    print('PDB Code: %s' % ppdb.code)
    print('PDB Header Line: %s' % ppdb.header)


class FileChooserWindow(Gtk.Window):
    def __init__(self):
        Gtk.Window.__init__(self, title="Escoge Pdb")
        self.show_all()
        box = Gtk.Box(spacing=6)
        self.add(box)

        button1 = Gtk.Button(label="Escoger pymol")
        button1.connect("clicked", self.on_pymol_clicked)
        box.add(button1)

        button2 = Gtk.Button(label="Escoger datos")
        button2.connect("clicked", self.on_datos_clicked)
        box.add(button2)

        button3 = Gtk.Button(label="Mostrar datos")
        button3.connect("clicked", self.on_datos_m_clicked)
        box.add(button3)

        button4 = Gtk.Button(label="Atras")
        button4.connect("clicked", self.on_atras_clicked)
        box.add(button4)

        button5 = Gtk.Button(label="Mostrar Creditos")
        button5.connect("clicked", self.about_cb)
        box.add(button5)

        button6 = Gtk.Button(label="Cancelar")
        button6.connect("clicked", self.on_cancel_clicked)
        box.add(button6)


    def on_atras_clicked(self, widget):
        print("Atras!")
        lugar_1 = variable
        lugar_2 = open_file(2)

        if lugar_1 == '' and lugar_2 != '':
            self.hide()
            import carpeta

        else:
            self.hide()
            import fichero


    def about_cb(self, widget):
        import autor

    def on_pymol_clicked(self, widget):
        ver_en_pymol()

    def on_datos_clicked(self,widget):
        ver_normal()

    def on_datos_m_clicked(self,widget):
        ver_datos()

    def on_cancel_clicked(self,widget):
        self.destroy()




win = FileChooserWindow()
win.connect("destroy", Gtk.main_quit)
win.show_all()
Gtk.main()
