#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import gi

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk
"""
funcion encargada de guardar la direccion del archivo
"""
def guardar_direccion(direccion, numero):
    #1 archivo lugar
    if numero == 1:
        file = open("direccion", "w")
        file.write(direccion)
        file.close()
    #2 carpeta lugar
    elif numero == 2:
        file = open("direccion2", "w")
        file.write(direccion)
        file.close()
"""
Funcion que se encarga de los archivos y ver su contenido
"""
def open_file(numero):
    #1 lugar del archivo
    if numero == 1:
        file = open("direccion","r")
        lugar = file.read()
        file.close()
    #2 lugar de la carpeta
    elif numero == 2:
        file = open("direccion2","r")
        lugar = file.read()
        file.close()
    return lugar

class FileChooserWindow(Gtk.Window):
    def __init__(self):

        guardar_direccion("",1)
        guardar_direccion("",2)
        print("datos borrados")

        Gtk.Window.__init__(self, title="Escoge Pdb")

        box = Gtk.Box(spacing=6)
        self.add(box)

        button1 = Gtk.Button(label="Escoger archivo")
        button1.connect("clicked", self.on_file_clicked)
        box.add(button1)

        button2 = Gtk.Button(label="Escoger carpeta")
        button2.connect("clicked", self.on_folder_clicked)
        box.add(button2)

        button3 = Gtk.Button(label="Mostrar Creditos")
        button3.connect("clicked", self.about_cb)
        box.add(button3)

    #se encarga de dar los creditos
    def about_cb(self, widget):
        import autor

    def on_file_clicked(self, widget):
        dialog = Gtk.FileChooserDialog(
            title="Pls elige", parent=self, action=Gtk.FileChooserAction.OPEN
        )
        dialog.add_buttons(
            Gtk.STOCK_CANCEL,
            Gtk.ResponseType.CANCEL,
            Gtk.STOCK_OPEN,
            Gtk.ResponseType.OK,
        )

        self.add_filters(dialog)

        response = dialog.run()
        if response == Gtk.ResponseType.OK:
            print("Open clicked")
            print("File selected: " + dialog.get_filename())
            nombre_pdb = dialog.get_filename()
            guardar_direccion(nombre_pdb, 1)
            import pdb
        elif response == Gtk.ResponseType.CANCEL:
            print("Cancel clicked")

        dialog.destroy()

    def add_filters(self, dialog):
        filter = Gtk.FileFilter()
        filter.set_name("PDB")
        filter.add_pattern("*.pdb")
        dialog.add_filter(filter)

        filter_any = Gtk.FileFilter()
        filter_any.set_name("Todo")
        filter_any.add_pattern("*")
        dialog.add_filter(filter_any)

    def on_folder_clicked(self, widget):
        dialog = Gtk.FileChooserDialog(
            title="Elija la carpeta",
            parent=self,
            action=Gtk.FileChooserAction.SELECT_FOLDER,
        )
        dialog.add_buttons(
            Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL, "Select", Gtk.ResponseType.OK
        )
        dialog.set_default_size(800, 400)

        response = dialog.run()
        if response == Gtk.ResponseType.OK:
            nombre_carp = dialog.get_filename()
            print("Select clicked")
            print("Folder selected: " + nombre_carp)
            guardar_direccion(nombre_carp, 2)
            import carpeta
        elif response == Gtk.ResponseType.CANCEL:
            print("Cancel clicked")

        dialog.destroy()




win = FileChooserWindow()
win.connect("destroy", Gtk.main_quit)
win.show_all()
Gtk.main()
