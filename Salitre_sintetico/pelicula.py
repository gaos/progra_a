#!/usr/bin/env python3
# -*- coding: utf-8 -*-

class movie:
    """Hecho para objeto pelicula"""

    def __init__(self, _nombre, _ano, _genero_peli, _votos, _duracion, _descri):

        self.__nombre = _nombre
        self.__ano = _ano #año
        self.__generos = _genero_peli 
        self.__duracion = _duracion
        self.__descri = _descri
        self.__votos = _votos
        self.__posibilidad = 0

#solo posibilidades es algo cambiante, por ello solo es atributo tiene set
    def set_posibilidad(self, posi):
        self.__posibilidad = posi
        return self.__posibilidad

    def get_ano(self):
        return self.__ano

    def get_posi(self):
        return self.__posibilidad
    def get_descri(self):
        return self.__descri

    def get_duracion(self):
        return self.__duracion

    def get_nombre(self):
        return self.__nombre

    def get_generos(self):
        return self.__generos

    def get_votos(self):
        return self.__votos
