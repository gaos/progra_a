#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import csv
from pelicula import movie
import os

#funcion para cargar el archivo csv
def load_file():

    with open('IMDb movies.csv') as csv_file:
        matriz = list(csv.reader(csv_file))

    return matriz

#funcion encargada de obtener una columna fija
def obtener_col(matriz, columna):

    lista_col = []

    for i in range(len(matriz)):
        if(i != 0):
            lista_col.append(matriz[i][columna])

    return lista_col

#funcion que se encarga de juntar los generos
def genero_unico(listado, genero2):

    lista = listado.split(", ")
    for i in range(len(lista)):
        genero2.append(lista[i])
    return genero2
"""
obtiene solamente los datos que se usaran al mostrar la peliculas
"""
def creadora_peli(matriz):

    blockbuster = [] #no es netflix, no tiene series

    col_nombre = obtener_col(matriz, 1)
    col_genero = obtener_col(matriz, 5)
    col_year = obtener_col(matriz, 3)
    col_duration = obtener_col(matriz, 6)
    col_votos = obtener_col(matriz, 14)
    col_descri = obtener_col(matriz, 13)

    for i in range(len(col_nombre)):
        peli = movie(col_nombre[i], col_year[i], col_genero[i], col_votos[i],
                        col_duration[i], col_descri[i])

        blockbuster.append(peli)

    return blockbuster

"""
funcion encargada de realizar una encuesta de los gustos
presenta 3 opciones, la de incluir el genero, False por si le disgusta ese
genero y None es le da igual si lo tiene o no.
"""
def encuesta_gender(generos):

    gusto = []

    for i in range(len(generos)):

        print("¿Te gusta el genero:{0} ?".format(generos[i]))
        print("S, para si, N, para no, no le importa, presione cualquier tecla")
        opc = str(input(":"))

        if opc.upper() == 'S':
            gusto.append(generos[i])

        elif(opc.upper() == 'N'):
            gusto.append(False)

        else:
            gusto.append(None)
    return gusto

"""
se dedica a comparar los generos de las peliculas y incluye las peliculas
con los gustos del usuario en una lista, ademas añade cuantos generos de los
que le gustan presenta el usuario
"""
def compara_genero(peliculas, peli_filtra, gusto, genero):

    #split utilizado genera una lista, debido a que los generos estan como str
    lista = peliculas.get_generos().split(", ")
    flag = True
    contador = 0

    for i in range(len(gusto)):
        for j in range(len(lista)):

            if lista[j] == genero[i] and gusto[i] == False:
                flag = False

            elif(lista[j] == genero[i] and gusto[i] != None and
                    gusto[i] != False):
                contador = contador + 1

    if flag == True:
        peliculas.set_posibilidad(contador)
        peli_filtra.append(peliculas)

#funcion encargada de mostrar las peliculas
def muestra(peli):

    print("\n\t", peli.get_nombre())
    print("Año:", peli.get_ano())
    print("Generos de la pelicula:", peli.get_generos())
    print("Generos que le gustan de la pelicula", peli.get_posi())
    print("Duración:", peli.get_duracion())
    print("Descripción:\n", peli.get_descri())
    print("Votos de usuarios:", peli.get_votos())


def main():

    genero = []
    genero2 = []
    peli_filtra = []


    matriz = load_file()
    peliculas = creadora_peli(matriz)

"""
Se trabajo una columna en vez de verificar en los objetos debido a que se
tendría que crear una nueva función para observar los generos, por cantidad
de lineas preferí re-usar"""
    col_gender = obtener_col(matriz, 5)

    col_gender2 = list(set(col_gender))

    #se llama a la función para juntar las listas
    for i in range(len(col_gender2)):
        genero2 = genero_unico(col_gender2[i], genero2)

    #se encarga que no haya duplicado de los generos
    genero = list(set(genero2))

    for i in range(len(genero)):
        print(genero[i], "columna", i)

    gusto = encuesta_gender(genero)

    for i in range(len(peliculas)):
        compara_genero(peliculas[i], peli_filtra, gusto, genero)


    os.system('clear')
    contador_pag = 0

    if(len(peli_filtra) != 0):

        for i in range(len(peli_filtra)):
            muestra(peli_filtra[i])

        #se encarga de solo mostrar 20, para que no se pierda datos mostrados
            if(i%20 == 0 and i != 0):

                contador_pag = contador_pag + 1
                print("Pagina {0} de {1}".format(contador_pag,
                                                    len(peli_filtra)/20))

                opc = str(input("¿desea avanzar a la siguiente pagina? s, si:"))

                if opc.upper() != 'S':
                    break
                else:
                    os.system('clear')

        print(len(peli_filtra))

"""else para el caso de que no exista por el filtrado, ejemplo solo le gusta
documentales y todos los demas generos no le gustan
"""
    else:
        print("Lamentamos informar que no existe de su gusto en el listado")

if __name__ == '__main__':
    main()
