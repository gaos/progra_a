#!/usr/bin/env python3
# -*- coding: utf-8 -*-

class cuenta:
    def __init__(self, cuenta_rut, nombre):
        self._rut = cuenta_rut
        self._nombre = nombre
        self._saldo = 0

#se encarga de cambiar, como un set
    def cambiar_saldo(self, saldo):
        self._saldo = saldo

#se encarga de entregar los atributos
    def mostrar(self):
        return self._saldo ,self._nombre, self._rut
