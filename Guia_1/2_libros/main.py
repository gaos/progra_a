#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from class_libros import libros

def main():
    lista_libros = []

    while True:

        libro = str(input("Ingrese el titulo:"))
        autor = str(input("Ingrese el nombre del autor:"))

        #while para obtener los ejemplares y que los prestados sean menor
        while True:
            try:
                ejemplares = int(input("Ingrese el numero de ejemplares:"))
                prestado = int(input("Ingrese el numero de libros prestados:"))
                if(prestado >= 0 and ejemplares > 0 and ejemplares > prestado):
                    break
            except:
                print("Vuelva a intentarlo con un numero entero positivo")
        libreria = libros(libro)
        libreria.set_autor(autor)
        libreria.set_numero(ejemplares)
        libreria.set_prestados(prestado)
        lista_libros.append(libreria)
        print(libreria)
        print(lista_libros)

        opc = str(input("Presione boton de apagado o x para parar:"))

        if opc.upper() == 'X':
            break

    print("Los libros son:")

    #recorre una lista para obtener los nombres de los libros
    for i in range(len(lista_libros)):
        temp = lista_libros[i]
        print(temp.get_libro())

if __name__ == '__main__':
    main()
