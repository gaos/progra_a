#!/usr/bin/env python3
# -*- coding: utf-8 -*-

class libros:

    def __init__(self, nombre):
        self.__nombre__ = nombre
        self.__autor = None
        self.__numero_ejemplares = None
        self.prestado = None

    def set_autor(self, autor):
        self.__autor = autor

    def set_numero(self, numero_ejemplares):
        self.__numero_ejemplares = numero_ejemplares

    def set_prestados(self, _prestados):
        self.prestado = _prestados

    def get_libro(self):
        return self.__nombre__
