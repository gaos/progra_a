#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from Clase_Tarjeta import tarjeta

def main():

    while(True):

        #name se encarga de optener el numero
        name = str(input("Ingrese el nombre del usuario de la tarjeta:"))
        persona = tarjeta(name)
        while True:
            try:
                #para que coherentemente un saldo positivo
                saldo = float(input("Ingrese el saldo:"))
                if saldo >=0:
                    break
            except:
                print("Vuelva a escribir un saldo correspondiente")

        persona.set_saldo(saldo)

        print("Nombre:{0}\nSaldo:{1}".format(persona.get_titular(),
                                                persona.get_saldo()))

        opc = str(input("Si desea parar presione el boton de apagado o x:"))
        if opc.upper() == 'X':
            break

if __name__ == '__main__':
    main()
