#!/usr/bin/env python3
# -*- coding: utf-8 -*-

class tarjeta:

    def __init__(self, _nombre):
        self.nombre = _nombre
        self.saldo = False

    def set_saldo(self, dinero):
        self.saldo = dinero

    def set_titular(self, titular):
        self.nombre = titular

    def get_titular(self):
        return self.nombre

    def get_saldo(self):
        return self.saldo
