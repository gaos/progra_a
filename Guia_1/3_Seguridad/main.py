#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from class_security import security

def main():

    while True:

        try:

            digitos = int(input("presione los 4 digitos:"))

            if(digitos < 10000 and digitos >= 1000):

                lista = []
                lista.append(list(str(digitos)))
#obtiene los numeros como lista con coordenadas especificas
                a1 = lista[0][0]
                b1 = lista[0][1]
                c1 = lista[0][2]
                d1 = lista[0][3]

                encriptado = security(a1, b1, c1, d1)
                print(encriptado)
                a1 = encriptado.get_a1()
                b1 = encriptado.get_b1()
                c1 = encriptado.get_c1()
                d1 = encriptado.get_d1()


                encriptado.set_cambio(a1,b1,c1,d1)

                a1 = encriptado.get_a1()
                b1 = encriptado.get_b1()
                c1 = encriptado.get_c1()
                d1 = encriptado.get_d1()
                print('encriptado:',a1,b1,c1,d1)

                #cambio
                encriptado.set_cambio(a1,b1,c1,d1)

                a1 = encriptado.get_a1()
                b1 = encriptado.get_b1()
                c1 = encriptado.get_c1()
                d1 = encriptado.get_d1()

                encriptado.desencriptar(a1,b1,c1,d1)

                a1 = encriptado.get_a1()
                b1 = encriptado.get_b1()
                c1 = encriptado.get_c1()
                d1 = encriptado.get_d1()
                print(a1,b1,c1,d1)

                break
        except:
            print("Ingrese un numero de 4 digitos")


if __name__ == '__main__':
    main()
