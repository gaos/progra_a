#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#funcion encargada de identificar el signo
def identificar_signo(a):
    if a < 0:
        return False
    else:
        return True


class security:

#constructor que define ya un poco encriptado
    def __init__(self,a,b,c,d):
        a = int(a)
        b = int(b)
        c = int(c)
        d = int(d)
        self._a = (a+7)%10
        self._b = (b+a+7)%10
        self._c = (c+b+a+7)%10
        self._d = (d+b+c+a+7)%10


    def set_cambio(self,a,b,c,d):
        self._a = c
        self._c = a
        self._b = d
        self._d = b

#funcion que se encarga de retornar sin encriptar, junto a set_cambio()
    def desencriptar(self,a,b,c,d):
        a1 = a
        b1 = b
        c1 = c
        d1 = d

        a = (a1 - 7)%10
        b = (b1 - 7 - a)%10
        c = (c1 - 7 - a - b)%10
        d = (d1 - 7 - a - b - c)%10

        flag = identificar_signo(a)

        if flag == True:
            self._a = a
        else:
            self._a = -a
        flag = identificar_signo(b)

        if flag == True:
            self._b = b
        else:
            self._b = -b

        flag = identificar_signo(c)

        if flag == True:
            self._c = c
        else:
            self._c = -c

        flag = identificar_signo(d)

        if flag == True:
            self._d = d
        else:
            self._d = -d

    def get_a1(self):
        return self._a
    def get_b1(self):
        return self._b
    def get_c1(self):
        return self._c
    def get_d1(self):
        return self._d
